<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:math="http://www.w3.org/2005/xpath-functions/math" xmlns:array="http://www.w3.org/2005/xpath-functions/array" xmlns:map="http://www.w3.org/2005/xpath-functions/map" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:err="http://www.w3.org/2005/xqt-errors" exclude-result-prefixes="array fn map math xhtml xs err" version="3.0">
	<xsl:output method="html" version="5.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/" name="xsl:initial-template">
		<html>
			<head>
				<title>XMLTest</title>
			</head>
			<!-- Well done Ayaz!  Your XSL properly formats and outputs the xml data.
You've also got alternating rows.
10/10
-->
			<body>
				<h2>Customer Info</h2> <br/>
				
				Name:<xsl:value-of select="telephoneBill/customer/name"/><br/>
				Address:<xsl:value-of select="telephoneBill/customer/address"/><br/>
				City:<xsl:value-of select="telephoneBill/customer/city"/><br/>
				Province:<xsl:value-of select="telephoneBill/customer/province"/><br/>
				
				<table cellpadding="5" border="1">
					<tbody>
						<tr>
							<th align="left">CalledNumber</th>
							<th align="left">Date</th>
							<th align="left">Duration in Minutes</th>
							<th align="left">Charge</th>
						</tr>
						<xsl:for-each select="telephoneBill/call">
							<tr>
							<!-- add some logic to change the bgcolor of even table rows -->
							<xsl:if test="position() mod 2=0"> <!-- if row number/2 gives 0 remainder -->
								<!-- position() is an xsl function that returns row number -->
								<xsl:attribute name="bgcolor">#EAEAEA</xsl:attribute>
							</xsl:if>
								<td><xsl:value-of select="attribute(number)"/></td>
								<td><xsl:value-of select="attribute(date)"/></td>
								<td><xsl:value-of select="attribute(durationInMinutes)"/></td>
								<td><xsl:value-of select="attribute(charge)"/></td>
							</tr>
						</xsl:for-each>
					</tbody>		
				</table>
			</body>
		</html>	
	</xsl:template>
</xsl:stylesheet>
